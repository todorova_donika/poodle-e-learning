from pydantic import BaseModel, constr, validator
import re

class UserRoles:
    INT_TO_STR = {1: 'student', 2: 'teacher', 3: 'admin'}
    # STR_TO_INT = {'student': 1, 'teacher': 2, 'admin': 3}

def _validate_password(value):
    pattern = '^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,24}$'
    if not re.match(pattern, value):
        raise ValueError('Password must be between 8 and 24 characters, contain at least one uppercase letter, one lowercase letter, one number and one special character.')

def _validate_email(value):
    pattern = '[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}'
    if not re.match(pattern, value):
        raise ValueError('Invalid email address.')
    
def _validate_phone(value):
    pattern = '^\+?1?\d{9,15}$'
    if not re.match(pattern, value):
        raise ValueError('Phone number must be in format +1234567890 or 1234567890')
    
def _validate_linkedin(value):
    pattern = '^(https?|ftp)://[^\s/$.?#].[^\s]*$'

    if not re.match(pattern, value):
        raise ValueError('Invalid LinkedIn profile link.')


class UserRegister(BaseModel): # helper model
    id: int | None
    email: str
    password: str
    deactivated: bool = False

    @validator('password')
    def password_must_be_valid(cls, value):
        _validate_password(value)
        return value
    
    @validator('email')
    def email_must_be_valid(cls, value):
        _validate_email(value)
        return value

class UserStudentRegister(UserRegister):
    first_name: constr(min_length=2, max_length=30)
    last_name: constr(min_length=2, max_length=30)

    class Config:
        schema_extra = {
        "example": {
                "email": "george_georgeson@gmail.com",
                "password": "p51M5%gAyAp",
                "first_name": "George",
                "last_name": "Georgeson"}}


class UserTeacherRegister(UserRegister):
    first_name: constr(min_length=2, max_length=30)
    last_name: constr(min_length=2, max_length=30)
    phone: str
    linked_in: str

    @validator('phone')
    def phone_must_be_valid(cls, value):
        _validate_phone(value)
        return value
    
    @validator('linked_in')
    def linked_in_must_be_valid(cls, value):
        _validate_linkedin(value)
        return value

    class Config:
        schema_extra = {
        "example": {
                "email": "johnjohnson@gmail.com",
                "password": "1qaz!QAZ",
                "first_name": "John",
                "last_name": "Johnson",
                "phone": "0895764324",
                "linked_in": "www.linkedin.com/in/john-johnson"}}


class UserDatabase(BaseModel):
    id: int | None
    email: str
    password: str
    role: str
    deactivated: bool = False

    @classmethod
    def from_query_result(cls, id, email, password, role, deactivated):
        return cls(
            id=id,
            email=email,
            password=password,
            role=UserRoles.INT_TO_STR[role], 
            deactivated=deactivated)


class StudentDatabase(UserDatabase):
    first_name: str
    last_name: str

    @classmethod
    def from_query_result(cls, id, email, password, role, deactivated, first_name, last_name):
        inherited = UserDatabase.from_query_result(id, email, password, role, deactivated).dict()
        return cls(**inherited,
            first_name=first_name,
            last_name=last_name)
    

class TeacherDatabase(UserDatabase):
    first_name: str
    last_name: str
    phone: str
    linked_in: str

    @classmethod
    def from_query_result(cls, id, email, password, role, deactivated, first_name, last_name, phone, linked_in):
        inherited = UserDatabase.from_query_result(id, email, password, role, deactivated).dict()
        return cls(**inherited,
            first_name=first_name,
            last_name=last_name,
            phone=phone,
            linked_in=linked_in)


class StudentUpdate(BaseModel):
    password: str | None
    deactivated: bool | None
    first_name: constr(min_length=2, max_length=30)
    last_name: constr(min_length=2, max_length=30)

    @validator('password')
    def validate_password(cls, value):
        _validate_password(value)
        return value

    @classmethod
    def from_query_result(cls, password, deactivated, first_name, last_name):
        return cls(
            password=password,
            deactivated=deactivated,
            first_name=first_name,
            last_name=last_name)
    
    class Config:
        schema_extra = {
        "example": {
                "password": "1qaz!QAZ",
                "deactivated": False,
                "first_name": "James",
                "last_name": "Jameson"}}
    

class TeacherUpdate(BaseModel):
    password: str | None
    deactivated: bool | None
    first_name: constr(min_length=2, max_length=30)
    last_name: constr(min_length=2, max_length=30)
    phone: str | None
    linked_in: str | None

    @validator('password')
    def validate_password(cls, value):
        _validate_password(value)
        return value
    
    @validator('phone')
    def validate_phone(cls, value):
        _validate_phone(value)
        return value
        
    @validator('linked_in')
    def validate_linked_in(cls, value):
        _validate_linkedin(value)
        return value
        
    @classmethod
    def from_query_result(cls, password, deactivated, first_name, last_name, phone, linked_in):
        return cls(
            password=password,
            deactivated=deactivated,
            first_name=first_name,
            last_name=last_name,
            phone=phone, 
            linked_in=linked_in)
    
    class Config:
        schema_extra = {
        "example": {
                "password": "1qaz!QAZ",
                "deactivated": False,
                "first_name": "James",
                "last_name": "Jameson",
                "phone": "+2378016677",
                "linked_in": "https://www.linkedin.com/in"}}


class StudentResponseModel(BaseModel):
    email: str
    role: str = 'student'
    deactivated: bool
    first_name: str
    last_name: str

    class Config:
        schema_extra = {
        "example": {
                "email": "ccccc@gmail.com",
                "deactivated": False,
                "first_name": "Bobby",
                "last_name": "Bobbson",}}


class TeacherResponseModel(BaseModel):
    email: str
    role: str = 'teacher'
    deactivated: bool
    first_name: str
    last_name: str
    phone: str
    linked_in: str


class AdminResponseModel(BaseModel):
    id: int | None
    email: str
    role: str = 'admin'
    deactivated: bool


class StudentInfoForAdmin(BaseModel):
    id: int
    email: str
    deactivated: bool
    rating: int

    @classmethod
    def query_result(cls, id, email, deactivated, rating):
        return cls(
            id = id,
            email = email,
            deactivated = deactivated,
            rating = rating)
    