from fastapi import APIRouter, HTTPException, status, Depends
from fastapi.responses import JSONResponse
from services import sections_service, courses_service
from pydantic import constr
from data.models import CourseWithSections, Section, SectionUpdate
from common import auth

sections_router = APIRouter(prefix='/courses/{course_id}/sections', tags=['Sections'])


@sections_router.get('/progress', responses={
    200: {"description": "Percentage of sections visited", "content": {"application/json": {"example": {"progress": 0.5}}}},
    404: {"description": "Course does not exist."},
    403: {"description": "You are not subscribed to this course."}})
def track_pogress(
    course_id:int,
    user_id: int = Depends(auth.student_only)):
    '''Return the percentage of sections visited from the student '''

    subscribed = courses_service.is_subscribed(user_id, course_id)
    if subscribed == False:
        raise HTTPException(status.HTTP_403_FORBIDDEN, detail="You are not subscribed to this course")
    
    progress_in_percentage = sections_service.track_course_progres(user_id, course_id)

    return progress_in_percentage


@sections_router.get('/', response_model=CourseWithSections, responses={
    200: {"description": "Course with its sections", "content": {"application/json": {"example": {"course": {}, "sections": []}}}},
    404: {"description": "Course does not exist."},
    403: {"description": "You are not subscribed to this course."}, 
    403: {"description": "You don't own this course."}})
def get_all_sections(
    course_id: int,
    sort: constr(regex='^asc|desc$') | None = "asc",
    sort_by: constr(regex='^title') | None = None,
    user: dict = Depends(auth.get_current_user)):
    '''Return course with its sections to a student subscribed to the course or to a teacher who owns the course'''

    course = courses_service.get_by_id(course_id)
    if not course:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Course does not exist")

    if user["role"] == "student":
        subscribed = courses_service.is_subscribed(user["user_id"], course_id)
        if subscribed == False:
            raise HTTPException(status.HTTP_403_FORBIDDEN, detail="You are not subscribed to this course")
    
    elif user["role"] == "teacher":
        owns = courses_service.is_owner(user["user_id"], course_id)
        if owns == False:
            raise HTTPException(status.HTTP_403_FORBIDDEN, detail="You don't own this course")

    sections = sections_service.all(course_id, user["user_id"], user["role"], sort, sort_by)

    return CourseWithSections(course=course, sections=sections)


@sections_router.post('/', response_model=Section, responses={
    201: {"description": "Section created", "content": {"application/json": {"example": {"message": "Section created"}}}},
    403: {"description": "You don't own this course"}})
def create_section(
    course_id:int,
    section: Section,
    user_id: int = Depends(auth.teacher_only)):
    '''Creates section if the teacher is owner of the course'''

    owns = courses_service.is_owner(user_id, course_id)
    if owns == False:
        raise HTTPException(status.HTTP_403_FORBIDDEN, detail="You don't own this course")
    
    result = sections_service.create(course_id, section)

    return result


@sections_router.put('/{section_id}', response_model=Section, responses={
    200: {"description": "Section updated", "content": {"application/json": {"example": {"message": "Section updated"}}}},
    403: {"description": "You don't own this course"},
    404: {"description": "Section does not exist"},
    404: {"description": "No section with this id in this course"}})
def update_section(
    course_id: int,
    section_id: int,
    section: SectionUpdate, #everything here should be optional
    user_id: int = Depends(auth.teacher_only)):
    '''Modifies section if the teacher is owner of the course'''

    owns = courses_service.is_owner(user_id, course_id)
    if owns == False:
        raise HTTPException(status.HTTP_403_FORBIDDEN, detail="You don't own this course")
    
    existing_section = sections_service.get_by_id(section_id)
    if not existing_section:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail='Section does not exist')

    section_in_course = sections_service.is_section_in_course(course_id, section_id)
    if section_in_course == False:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail=f'No such section in this course')

    result = sections_service.update(existing_section, section)

    return result


@sections_router.delete('/{section_id}', status_code=204, responses={
    204: {"description": "Section deleted", "content": {"application/json": {"example": {"message": "Section deleted"}}}},
    403: {"description": "You don't own this course"},
    404: {"description": "No such section in this course"}})
def delete_section(
    course_id:int,
    section_id: int,
    user_id: int = Depends(auth.teacher_only)):
    '''Deletes section if the teacher is owner of the course'''

    owns = courses_service.is_owner(user_id, course_id)
    if owns == False:
        raise HTTPException(status.HTTP_403_FORBIDDEN, detail="You don't own this course")

    section_in_course = sections_service.is_section_in_course(course_id, section_id)
    if section_in_course == False:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail=f'No such section in this course')

    return sections_service.delete(section_id)


@sections_router.post('/{section_id}/visited', status_code=201, responses={
    201: {"description": "Section marked as visited", "content": {"application/json": {"example": {"message": "Section marked as visited"}}}}, 
    403: {"description": "You are not subscribed to this course"},
    404: {"description": "No such section in this course"}})
def visit_section(
    course_id:int,
    section_id:int,
    user_id: int = Depends(auth.student_only)):
    '''Marks section as visited if the student is subscribed to the course'''

    subscribed = courses_service.is_subscribed(user_id, course_id)
    if subscribed == False:
        raise HTTPException(status.HTTP_403_FORBIDDEN, detail="You are not subscribed to this course")
    
    section_in_course = sections_service.is_section_in_course(course_id, section_id)
    if section_in_course == False:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail=f'No such section in this course')
    
    sections_service.visit_section(user_id, section_id)
    return JSONResponse(status_code=200, content={'message': "Section marked as visited"})

