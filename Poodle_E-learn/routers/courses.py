from fastapi import APIRouter, HTTPException, status, Depends, Query
from fastapi.responses import JSONResponse
from data.models import CourseCreate, CreateTags
from services import courses_service, tags_service, admin_service
from common import auth
from pydantic import conint, constr


PRIVATE_COURSES_LIMIT = 5

courses_router = APIRouter(prefix='/courses', tags=['Course'])


@courses_router.get('/public', responses={
    200: {"content": {"application/json": {
                        "example": {"total_courses": 30,
                                    "page": 1,
                                    "limit": 10,
                                    "courses": []}}}}})
def get_courses_as_anonymous(
    page: int = Query(1, ge=1),
    limit: int = Query(10, ge=1),
    sort: str | None = Query(None, regex='^asc|desc$'),
    sort_by: str | None = None,
    search: str | None = None,
    tags: str | None = None):

    '''Gets course info as an unregistered user for all public and not deactivated courses'''

    courses, total_courses = courses_service.get_all_public(page=page, limit=limit, search=search, tags=tags)
    if sort:
        courses = courses_service.sort_public_courses(courses, reverse=sort == 'desc',attribute=sort_by)

    return {
        "total_courses": total_courses,
        "page": page,
        "limit": limit,
        "courses": courses
    }



@courses_router.get('/', responses={
    200: {"content": {"application/json": {
                        "example": {"total_courses": 30,
                                    "page": 1,
                                    "limit": 10,
                                    "courses": []}}}}})
def get_all_courses(
    page: int = Query(1, ge=1),
    limit: int = Query(10, ge=1),
    sort: constr(regex='^asc|desc$') | None = None,
    sort_by: str | None = None,
    search: str | None = None,
    tags: str | None = None,
    user: dict = Depends(auth.get_current_user)):
    '''Gets detailed course info as a registered user for all not deactivated courses'''

    courses, total_courses  = courses_service.get_all(page=page, limit=limit, search=search, tags=tags)
    if sort:
        courses= courses_service.sort_courses(courses, reverse=sort == 'desc', attribute=sort_by)

    return {
        "total_courses": total_courses,
        "page": page,
        "limit": limit,
        "courses": courses
    }



@courses_router.get('/subscribed', responses={
    200: {"content": {"application/json": {
                        "example": {"total_courses": 30,
                                    "page": 1,
                                    "limit": 10,
                                    "courses": []}}}}})
def get_subscribed_courses(
    page: int = Query(1, ge=1),
    limit: int = Query(10, ge=1),
    sort: constr(regex='^asc|desc$') | None = None,
    sort_by: str | None = None,
    search: str | None = None,
    tags: str | None = None,
    user: dict = Depends(auth.get_current_user)):
    '''Gets detailed course info as a registered user for all courses the user is subscribed to'''

    courses = courses_service.get_subscribed_courses(user=user, search=search, tags=tags)
    if sort:
        courses = courses_service.sort_courses(courses, reverse=sort == 'desc', attribute=sort_by)

    total_courses = len(courses)
    start_index = (page - 1) * limit
    end_index = start_index + limit
    paginated_courses = courses[start_index:end_index]

    return {
        "total_courses": total_courses,
        "page": page,
        "limit": limit,
        "courses": paginated_courses
    }



@courses_router.put('/{course_id}/unsubscribe', responses={
    200: {"content": {"application/json": {
                        "example": {"message": "Successfully unsubscribed from the course."}}}},
    404: {"description": "No course with id: {course_id} exists."},
    400: {"description": "The user is not subscribed to course id: {course_id}."}})
                           
def unsubscribe_from_course(
    course_id: int,
    user_id: int = Depends(auth.student_only)):
    '''Unsubscribes student from a course'''

    # Check if the course exists
    course = courses_service.get_by_id(course_id)
    if not course:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'No course with id: {course_id} exists.')
    
    # Check if the user is subscribed to this course
    user_subscribed = courses_service.student_is_subscribed(course_id, user_id)
    if not user_subscribed:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f'The user is not subscribed to course id: {course_id}.')
    
    # # Check if the course is private
    # if course.type != 'private':
    #     raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Cannot unsubscribe from a non-private course.')

    # Unsubscribe the student from the course
    courses_service.unsubscribe_student(course_id, user_id)

    return {'message': 'Successfully unsubscribed from the course.'}



@courses_router.put('/{course_id}/subscribe', responses={
    200: {"content": {"application/json": {
                        "example": {"message": "Successfully subscribed to the course."}}}},
    404: {"description": "No course with id: {course_id} exists."},
    400: {"description": "The user is already subscribed to course id: {course_id}."}})

def subscribe_to_course(
    course_id: int,
    user_id: int = Depends(auth.student_only)):
    '''Subscribes students to a maximum of 5 premium courses at a time and unlimited number of public courses'''

    # Check if the course exists
    course = courses_service.get_by_id(course_id)
    if not course:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'No course with id: {course_id} exists.')

    # Check if the user is already subscribed to the course
    user_subscribed = courses_service.student_is_subscribed(course_id, user_id)
    if user_subscribed:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f'The user is already subscribed to course id: {course_id}.')

    # Check if the user subscription subscribed is pending
    user_subscribed = courses_service.subscription_is_pending(course_id, user_id)
    if user_subscribed:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f'The user subscription status is pending for course id: {course_id}.')

    # Check the subscription limit for premium courses
    if course.type == 'private':
        private_courses_limit = courses_service.get_student_limit(user_id)
        if private_courses_limit >= PRIVATE_COURSES_LIMIT: # make a separate const for the limit
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Maximum limit reached for subscribing to private courses.')

    # Subscribe the student to the course
    courses_service.subscribe_student(course_id, user_id)

    return JSONResponse(status_code=200, content = {'message': 'You have successfully applied for this course; your subscription is pending.'})



@courses_router.post('/', status_code=status.HTTP_201_CREATED, responses={
    201: {"description": "Course created successfully."},
    400: {"description": "Course with title: {course.title} already exists."}})
def create_course(
    course: CourseCreate, 
    user_id: int = Depends(auth.teacher_only)):
    '''Teacher creates new course'''

    # Check if course already exists (by title)
    course_exists = courses_service.exists(course.title)
    if course_exists:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f'Course with title: {course.title} already exists.')    

    # Create the course in the database
    course = courses_service.create(course, user_id)

    return course



@courses_router.put('/{course_id}/enroll/{student_id}', responses={
    200: {"content": {"application/json": {
                        "example": {"message": "Successfully enrolled student to the course."}}}},
    404: {"description": "No course with id: {course_id} exists."},
    400: {"description": "The user is already subscribed to course id: {course_id}."}})

def approve_enrollment_request_or_batch_approve(
    course_id: int,
    student_id: int,
    user_id: int = Depends(auth.teacher_only)):
    '''Teachers approves enrollment requests by student id, otherwise batch approves all enrollment requests if student_id=0'''

    # Check if the course exists
    course = courses_service.get_by_id(course_id)
    if not course:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'No course with id: {course_id} exists.')

    # Check if the user is the owner of the course
    user_is_owner = courses_service.is_owner(course_id, user_id)
    if not user_is_owner:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='You are not the owner of this course.')
    
    if student_id != 0:
        # Check if the student has sent an enrollment request for this course
        enrollment_request = courses_service.get_enrollment_request(course_id, student_id)
        if not enrollment_request:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='No enrollment request found for this student.')

        # Approve the enrollment request
        courses_service.approve_enrollment_request(course_id, student_id)
        return {'message': f'Enrollment request for student: {student_id} approved.'}
    
    else:
        # Approves all enrollment requests for this course if student_id = 0
        result = courses_service.approve_all_requests(course_id)
        return {'message': f'Students with ids: {result} are enrolled in the course.'}
    


@courses_router.post('/{course_id}')
def rate_course(
    course_id: int, 
    rating: conint(ge=1, le=10), 
    user_id = Depends(auth.student_only)
    ):
    '''Student rates a course that they is subscribed to'''

    if not courses_service.student_is_subscribed(course_id, user_id):
        raise HTTPException(status_code=403, detail='You are not subscribed to this course and, therefore, cannot rate it.')

    courses_service.rate(course_id, rating, user_id)

    return {"course_rating": courses_service.aggregate_ratings(course_id)}



@courses_router.post('/{course_id}/tags')
def assign_tags_to_course(
    course_id: int, 
    tags: CreateTags, 
    user_id: int = Depends(auth.teacher_only)):
    '''Teacher assigns tags to an existing course'''

    # Check if the course exists
    course = courses_service.get_by_id(course_id)
    if not course:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'No course with id: {course_id} exists.')

    # Check if the user is the owner of the course
    user_is_owner = courses_service.is_owner(course_id, user_id)
    if not user_is_owner:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='You are not the owner of this course.')

    # Create or fetch existing tags
    created_tags = []
    for tag_name in tags.tags:
        tag = tags_service.get_tag_by_name(tag_name)
        if not tag:
            tag = tags_service.create_tag(tag_name)
        created_tags.append(tag)

    # Assign tags to the course
    tags_service.assign_tags_to_course(course_id, created_tags)
    response = courses_service.get_by_id(course_id)

    return response


@courses_router.get('/{course_id}/subscriptions', responses={
    200: {"content": {"application/json": {
                        "example": {"total_students": 10,
                                    "page": 1,
                                    "limit": 10,
                                    "students": []}}}},
    404: {"description": "No course with id: {course_id} exists."}})

def get_students_who_subscribed_to_course(
    course_id: int,
    page: int = Query(1, ge=1),
    limit: int = Query(10, ge=1),
    sort: str|None = Query(None, regex='^asc|desc$'),   
    user_id: int = Depends(auth.teacher_only)):
    '''Returns past and present students who subscribed to the course'''

    # Check if the course exists
    course = courses_service.get_by_id(course_id)
    if not course:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'No course with id: {course_id} exists.')

    # Check if the user is the owner of the course
    user_is_owner = courses_service.is_owner(course_id, user_id)
    if not user_is_owner:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='You are not the owner of this course.')

    courses = courses_service.get_students_who_subscribed_to_course(course_id, sort=sort)

    total_courses = len(courses)
    start_index = (page - 1) * limit
    end_index = start_index + limit
    paginated_courses = courses[start_index:end_index]

    return {
        "total_students": total_courses,
        "page": page,
        "limit": limit,
        "students": paginated_courses
    }