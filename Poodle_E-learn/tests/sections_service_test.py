import sys
import os
from unittest import TestCase
from unittest.mock import Mock, create_autospec, patch

# Add the project root directory to the Python module search path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from data.models import Section, SectionForStudent, SectionUpdate
from services import sections_service


section_update = SectionUpdate(title='title1', content_type ='content_type1', description ='description', info ='info')  
section_update_db = ('title1', 'content_type1', 'description', 'info')

section1 = Section(id=1, title='title1', content_type ='content_type1', description ='description1', info ='info1')  
section1_db = (1, 'title1', 'content_type1', 'description1', 'info1')  

section2 = Section(id=2, title='title2', content_type ='content_type2', description ='description2', info ='info2') 
section2_db = (2, 'title2', 'content_type2', 'description2', 'info2') 

section_for_student1 = SectionForStudent(id=1, title='title1', content_type ='content_type1', description ='description1', info ='info1', visited=None)
section_for_student1_db = (1, 'title1', 'content_type1', 'description1', 'info1', None)

section_for_student2 = SectionForStudent(id=2, title='title2', content_type ='content_type2', description ='description2', info ='info2', visited=True)
section_for_student2_db = (2, 'title2', 'content_type2', 'description2', 'info2', True)

class SectionService_Should(TestCase):

    @patch('services.sections_service.database', autospec=True)
    def test_all_returnsListOfSections_whenTeacher(self, mock_db):
        mock_db.read_query.return_value = [section1_db, section1_db]
        result = list(sections_service.all(1,2,'teacher'))
        self.assertEqual(2, len(result))

    @patch('services.sections_service.database', autospec=True)
    def test_all_returnsListOfSections_whenStudent(self, mock_db):
        mock_db.read_query.return_value = [section_for_student1_db, section_for_student2_db]
        result = list(sections_service.all(1,2,'student'))
        self.assertEqual(2, len(result))

    @patch('services.sections_service.database', autospec=True)
    def test_all_returnsListOfSections_whenNoData(self, mock_db):
        mock_db.read_query.return_value = []
        result = list(sections_service.all(1,2,'student'))
        self.assertEqual(0, len(result))


    @patch('services.sections_service.database', autospec=True)
    def test_get_by_id_returnsSection_whenData(self, mock_db):
        mock_db.read_query.return_value = [section1_db]
        result = sections_service.get_by_id(1)
        expected = section1
        self.assertEqual(expected, result)

