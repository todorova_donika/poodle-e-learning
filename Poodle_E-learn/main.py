from fastapi import FastAPI
from routers.users import users_router
from routers.courses import courses_router
from routers.sections import sections_router
from routers.tags import tags_router
from routers.admin import admin_router
# from data.database import init_database 
# init_database()

app = FastAPI()
app.include_router(users_router)
app.include_router(courses_router)
app.include_router(sections_router)
app.include_router(tags_router)
app.include_router(admin_router)

