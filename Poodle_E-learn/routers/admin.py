from fastapi import APIRouter, HTTPException, Query, status, Depends
from services import admin_service, courses_service, users_service
from fastapi.responses import JSONResponse
from common import auth

admin_router = APIRouter(prefix='/admin', tags=['Admin'])


@admin_router.get('/courses', responses={
    200: {"content": {"application/json": {
                        "example": {"total_courses": 30,
                                    "page": 1,
                                    "limit": 10,
                                    "courses": []}}}}})

def get_all_courses(
    page: int = Query(1, ge=1),
    limit: int = Query(10, ge=1),
    sort: str|None = Query(None, regex='^asc|desc$'),
    search: str | None = None,
    admin_id: int = Depends(auth.admin_only)):
    '''Returns all courses and the number of students in them'''

    courses, total_courses = admin_service.get_all(search=search, page=page, limit=limit, sort=sort)

    return {
        "total_courses": total_courses,
        "page": page,
        "limit": limit,
        "courses": courses
    }

    
@admin_router.get('/courses/{course_id}/ratings', responses={
    200: {"content": {"application/json": {
                        "example": {"total_students": 15,
                                    "page": 1,
                                    "limit": 10,
                                    "students": []}}}},
    404: {"description": "No course with id: {course_id} exists."}})

def get_students_who_rated_course(
    course_id: int,
    page: int = Query(1, ge=1),
    limit: int = Query(10, ge=1),
    sort: str|None = Query(None, regex='^asc|desc$'),   
    admin_id: int = Depends(auth.admin_only)):
    '''Returns past and present students who rated the course'''

    # Check if the course exists
    course = courses_service.get_by_id(course_id)
    if not course:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'No course with id: {course_id} exists.')

    courses = admin_service.get_students_who_rated_course(course_id, sort=sort)

    total_courses = len(courses)
    start_index = (page - 1) * limit
    end_index = start_index + limit
    paginated_courses = courses[start_index:end_index]

    return {
        "total_students": total_courses,
        "page": page,
        "limit": limit,
        "students": paginated_courses
    }


@admin_router.put('/courses/{course_id}/status', responses={
    200: {"content": {"application/json":{"example":{"message":'Status chaged to deactivated.'}}}},
    404: {"description": "No course with id: {course_id} exists."},
    404: {"description": "No course with id: {course_id} exists."},
    409: {"description": "Course with id: {course_id} has students in it."}})

def change_course_status(
    course_id:int, 
    admin_id: int = Depends(auth.admin_only)
    ):
    '''Deactivate/activate course'''

    # Check if the course exists
    course = courses_service.get_by_id(course_id)
    if not course:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'No course with id: {course_id} exists.')
    
    # Check if the course has students
    count_students = admin_service.get_students_in_course(course_id)
    if count_students > 1:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=f'Course with id: {course_id} has students in it.')
    
    # Deactivate/activate course
    course_status = admin_service.change_course_status(course_id)

    return JSONResponse(status_code=200, content={"message":f'Status chaged to {course_status}.'})


@admin_router.put('/students/{user_id}/status', responses={
    200: {"content": {"application/json":{"example":{"message":'Status chaged to deactivated.'}}}},
    404: {"description": "No user with id: {user_id} exists."},
    404: {"description": "No student with id: {user_id} exists."}})

def change_student_status(
    user_id: int, 
    admin_id: int = Depends(auth.admin_only)
    ):
    '''Deactivate/activate student's account'''

    # Check if the student exists
    user = users_service.get_by_id(user_id)
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'No user with id: {user_id} exists.')
    if user.role != "student":
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'No student with id: {user_id} exists.')
    
    # Deactivate/activate student account
    student_status = admin_service.change_student_account_status(user_id)

    return JSONResponse(status_code=200, content={'message': f'Status chaged to {student_status}.'})

@admin_router.delete('/courses/{course_id}/students/{user_id}', status_code=204, responses={
    204: {"description": "No content", "content": {"application/json":{"example":
         {'message': 'Student with id {user_id} is deleted from course with id {course_id}.'}}}},
    404: {"description": "No student with id: {user_id} exists."},
    404: {"description": "No course with id: {course_id} exists."},
    404: {"description": "The user is not subscribed or pending to course with id: {course_id}."}})

def change_student_status(
    course_id:int,
    user_id: int, 
    admin_id: int = Depends(auth.admin_only)
    ):
    '''Delete student from course'''
    
    # Check if the student exists
    user = users_service.get_by_id(user_id)
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'No user with id: {user_id} exists.')
    if user.role != "student":
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'No student with id: {user_id} exists.')
    
    # Check if the course exists
    course = courses_service.get_by_id(course_id)
    if not course:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'No course with id: {course_id} exists.')
    
    # Check if the user is subscribed to the course 
    user_subscribed = admin_service.is_subscribed_or_pending(course_id, user_id)
    if not user_subscribed:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f'The user is not subscribed or pending to course id: {course_id}.')

    # Delete student from course
    admin_service.delete_student_from_course(course_id, user_id)

    return JSONResponse(status_code=200, content={'message': f'Student with id {user_id} is deleted from course with id {course_id}.'})






