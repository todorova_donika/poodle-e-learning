from data import database


def get_tag_by_name(tag_name: str) -> int | None:
    '''Returns the id of the tag'''
    data = database.read_query('''
            SELECT id FROM tags WHERE name LIKE ?''', (tag_name,))    

    if len(data) > 0:
        return data[0][0]
    return None


def create_tag(tag_name: str) -> int:
    '''Creates a new tag and returns its id'''

    query = '''
            INSERT INTO tags(name) VALUES (?)'''   
    params = (tag_name,)
    data = database.insert_query(query, params)    
    return data


def assign_tags_to_course(course_id: int, created_tags: list[str]) -> int:
    '''Assigngns one or multiple tags to already existing course and returns their id-s'''

    database.update_query('''DELETE FROM courses_has_tags WHERE courses_id =?''', (course_id,))
    query = '''
            INSERT INTO courses_has_tags (courses_id, tags_id)
            VALUES 
    '''
    temp = []
    for tag in created_tags:
        temp.append(f'({course_id}, {tag})')
    query+=','.join(temp)
    database.insert_query(query)


def count_all_tags() -> int:
    '''Returns the number of all available tags'''

    query = '''
            SELECT COUNT(*) AS total_tags
            FROM tags
            '''
    result = database.query_count(query)
    total_tags = result if result else 0
    return total_tags


def get_all_tags(offset: int, page_size: int) -> list:
    '''Returns a list of all available tags'''

    data = database.read_query('''
            SELECT id, name FROM tags LIMIT ?, ?''', (offset, page_size))
    return data