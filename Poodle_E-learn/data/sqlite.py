from sqlite3 import connect

_DB_FILE = './data/dbfile.db'

def read_query(sql: str, sql_params=()):
    with connect(_DB_FILE) as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)

        return list(cursor)

def insert_query(sql: str, sql_params=()) -> int:
    with connect(_DB_FILE) as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()

        return cursor.lastrowid

def update_query(sql: str, sql_params=()) -> bool:
    with connect(_DB_FILE) as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()

        return cursor.rowcount > 0


def query_count(sql: str, sql_params=()):
    with connect(_DB_FILE) as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)

        return cursor.fetchone()[0]


def init_database():
    # Create tables
    with connect(_DB_FILE) as conn:
        cursor = conn.cursor()

        cursor.execute('''CREATE TABLE IF NOT EXISTS users (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        mail TEXT NOT NULL UNIQUE,
                        password TEXT NOT NULL,
                        role INTEGER NOT NULL,
                        deactivated INTEGER NOT NULL DEFAULT 0
                        );''')
        
        cursor.execute('''CREATE TABLE IF NOT EXISTS students (
                        users_id INTEGER PRIMARY KEY,
                        first_name TEXT NOT NULL,
                        lastname TEXT NOT NULL,
                        FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE NO ACTION ON UPDATE NO ACTION
                        );''')
        
        cursor.execute('''CREATE TABLE IF NOT EXISTS teachers (
                        users_id INTEGER NOT NULL,
                        first_name TEXT NOT NULL,
                        last_name TEXT NOT NULL,
                        phone TEXT NOT NULL,
                        linked_in TEXT NOT NULL,
                        PRIMARY KEY (users_id),
                        FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE NO ACTION ON UPDATE NO ACTION
                        );''')
        
        cursor.execute('''CREATE TABLE IF NOT EXISTS courses (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        title TEXT NOT NULL UNIQUE,
                        description TEXT NOT NULL,
                        objectives TEXT NOT NULL,
                        type INTEGER NOT NULL,
                        deactivated INTEGER NOT NULL DEFAULT 0,
                        home_page TEXT DEFAULT NULL);''')
        
        cursor.execute('''CREATE TABLE IF NOT EXISTS tags (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        name TEXT NOT NULL
                        );''')
        
        cursor.execute('''CREATE TABLE IF NOT EXISTS courses_has_tags (
                        courses_id INTEGER NOT NULL,
                        tags_id INTEGER NOT NULL,
                        PRIMARY KEY (courses_id, tags_id),
                        FOREIGN KEY (courses_id) REFERENCES courses (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
                        FOREIGN KEY (tags_id) REFERENCES tags (id) ON DELETE NO ACTION ON UPDATE NO ACTION
                        );''')
        
        cursor.execute('''CREATE TABLE IF NOT EXISTS sections (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        courses_id INTEGER NOT NULL,
                        title TEXT NOT NULL,
                        content_type TEXT NOT NULL,
                        description TEXT DEFAULT NULL,
                        external_info TEXT DEFAULT NULL,
                        FOREIGN KEY (courses_id) REFERENCES courses (id) ON DELETE NO ACTION ON UPDATE NO ACTION
                        );''')
        
        cursor.execute('''CREATE TABLE IF NOT EXISTS students_has_courses (
                        users_id INTEGER NOT NULL,
                        courses_id INTEGER NOT NULL,
                        subscription INTEGER NOT NULL,
                        PRIMARY KEY (users_id, courses_id),
                        FOREIGN KEY (courses_id) REFERENCES courses (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
                        FOREIGN KEY (users_id) REFERENCES students (users_id) ON DELETE NO ACTION ON UPDATE NO ACTION
                        );''')
        
        cursor.execute('''CREATE TABLE IF NOT EXISTS students_has_ratings (
                        users_id INTEGER NOT NULL,
                        courses_id INTEGER NOT NULL,
                        rating INTEGER NOT NULL,
                        PRIMARY KEY (users_id, courses_id),
                        FOREIGN KEY (courses_id) REFERENCES courses (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
                        FOREIGN KEY (users_id) REFERENCES students (users_id) ON DELETE NO ACTION ON UPDATE NO ACTION
                        );''')
        
        cursor.execute('''CREATE TABLE IF NOT EXISTS students_has_sections (
                        users_id INTEGER NOT NULL,
                        sections_id INTEGER NOT NULL,
                        visited INTEGER DEFAULT 1,
                        PRIMARY KEY (users_id, sections_id),
                        FOREIGN KEY (sections_id) REFERENCES sections (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
                        FOREIGN KEY (users_id) REFERENCES students (users_id) ON DELETE NO ACTION ON UPDATE NO ACTION
                        );''')
        
        cursor.execute('''CREATE TABLE IF NOT EXISTS teachers_has_courses (
                        users_id INTEGER NOT NULL,
                        courses_id INTEGER NOT NULL,
                        PRIMARY KEY (users_id,courses_id),
                        FOREIGN KEY (courses_id) REFERENCES courses (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
                        FOREIGN KEY (users_id) REFERENCES teachers (users_id) ON DELETE NO ACTION ON UPDATE NO ACTION
                        );''')
        

    # data seed
    if query_count('SELECT COUNT(*) from users') == 0:
        print('Inserting users')
        insert_query('''INSERT INTO users VALUES
                        (1,'dictum.placerat.augue@icloud.ca','6dbeeb3456db57818bb9b5a378d74e361d0898e822e07b4eb6df3080a9626a7b',3,0),
                        (2,'ridiculus@hotmail.org','b5529ac3f4fcbcf6a672605595ef1eddca44d0ccb5147359c4e2218326139910',2,0),
                        (3,'leocr@yahoo.org','52deb3fdc172e1910dbacce5cbab96090b253c03795644871b85d92d091e60de',2,0),
                        (4,'magna@protonmail.couk','13d9048973b9768da89c5c6fefca88e1313e948a492a0aa5a47cae9329052b94',1,0),
                        (5,'pretium@aol.edu','109f0ddf70137a22c8689c1c7102ae27d8caf360b45cc45d99eb8cb5cb65dcb9',1,0);''')
                                
    if query_count('SELECT COUNT(*) from students') == 0:
        print('Inserting students')
        insert_query('''INSERT INTO students VALUES
                        (4,'Grace','Woodward'),
                        (5,'Ann','Bowman');''')
        
    if query_count('SELECT COUNT(*) from teachers') == 0:
        print('Inserting teachers')
        insert_query('''INSERT INTO teachers VALUES
                        (2,'Jordan','Johnson','12345678','https://www.linkedin.com/feed/'),
                        (3,'Leonard','Cruz','87654321','https://www.linkedin.com/feed/');''')
        
    if query_count('SELECT COUNT(*) from courses') == 0:
        print('Inserting courses')
        insert_query('''INSERT INTO `courses` VALUES
                    (1,'Course 1','description course 1','objectives course 1',1,0,'asdf.com'),
                    (2,'Course 2','description course 2','objectives course 2',1,0,NULL),
                    (3,'Course 3 pr','description course 3','objectives course 3',2,0,'qwert.com'),
                    (4,'Course 4 pr','description course 4','objectives course 4',2,0,NULL),
                    (5,'Course 5','description course 5','objectives course 5',1,1,NULL),
                    (6,'Course 6 pr','description course 6','objectives course 6',2,0,NULL),
                    (7,'Course 7 pr','description course 7','objectives course 7',2,0,NULL),
                    (8,'Course 8 pr','description course 8','objectives course 8',2,0,NULL),
                    (9,'Course 9 pr','description course 9','objectives course 9',2,0,NULL);''')
        
    if query_count('SELECT COUNT(*) from tags') == 0:
        print('Inserting tags')
        insert_query('''INSERT INTO tags VALUES
                        (1,'tag 1'),
                        (2,'tag 2'),
                        (3,'tag 3'),
                        (4,'tag 4'),
                        (5,'tag 5');''')
        
    if query_count('SELECT COUNT(*) from courses_has_tags') == 0:
        print('Inserting courses_has_tags')
        insert_query('''INSERT INTO courses_has_tags VALUES
                        (1,1),
                        (1,2),
                        (2,2),
                        (3,1),
                        (3,3),
                        (4,4);''')
        
    if query_count('SELECT COUNT(*) from sections') == 0:
        print('Inserting sections')
        insert_query('''INSERT INTO sections VALUES
                        (1,1,'title 1a','content 11','description','asdg.com'),
                        (2,1,'title 1b','content 12',NULL,'asdg.com'),
                        (3,1,'title 13','content 13','description',NULL),
                        (4,1,'title 14','content 14',NULL,NULL),
                        (5,2,'title 2a','content 21','description','asdg.com'),
                        (6,2,'title 2b','content 22',NULL,'asdg.com'),
                        (7,3,'title 3B','content 31','description',NULL),
                        (8,4,'title 4x','content 41',NULL,NULL),
                        (9,4,'title 4y','content 42','description',NULL),
                        (10,3,'title 3A','content 32','','asdg.com');''')
        
    if query_count('SELECT COUNT(*) from students_has_courses') == 0:
        print('Inserting students_has_courses')
        insert_query('''INSERT INTO students_has_courses VALUES
                        (4,1,1),
                        (4,2,1),
                        (4,5,2),
                        (5,1,1),
                        (5,4,1),
                        (5,6,4);''')
        
    if query_count('SELECT COUNT(*) from students_has_ratings') == 0:
        print('Inserting students_has_ratings')
        insert_query('''INSERT INTO students_has_ratings VALUES
                        (4,1,10),
                        (4,2,6),
                        (4,5,2),
                        (5,1,8),
                        (5,3,7);''')
        
    if query_count('SELECT COUNT(*) from students_has_sections') == 0:
        print('Inserting students_has_sections')
        insert_query('''INSERT INTO students_has_sections VALUES
                        (4,1,1),
                        (4,2,1),
                        (5,1,1),
                        (5,8,1);''')
        
    if query_count('SELECT COUNT(*) from teachers_has_courses') == 0:
        print('Inserting teachers_has_courses')
        insert_query('''INSERT INTO teachers_has_courses VALUES
                        (2,1),
                        (2,2),
                        (2,5),
                        (2,6),
                        (2,9),
                        (3,3),
                        (3,4),
                        (3,7),
                        (3,8);''')
        
# PRAGMA foreign_keys = ON;