from data.models import CourseResponceModelAdmin 
from data.models_users import StudentInfoForAdmin
from data import database


def get_all(search:str=None, page:int=None, limit:int=None, sort:str=None) -> list[CourseResponceModelAdmin]:
    '''Returns all courses for admins'''

    offset = (page - 1) * limit
    query = '''
        SELECT c.id, c.title, c.description, c.objectives, c.type, t.tags, AVG(r.rating) AS rating, COUNT(DISTINCT sc.users_id) AS student_count
        FROM courses AS c
        LEFT JOIN (
            SELECT courses_id, GROUP_CONCAT(tags.name) AS tags
            FROM courses_has_tags
            LEFT JOIN tags ON tags.id = courses_has_tags.tags_id
            GROUP BY courses_id
        ) 
        AS t ON c.id = t.courses_id
        LEFT JOIN students_has_courses AS sc ON c.id = sc.courses_id
        LEFT JOIN students_has_ratings AS r ON c.id = r.courses_id
    '''
    query_cout = '''SELECT count(*) FROM courses AS c WHERE deactivated = 0'''

    if search:
        query += f' WHERE c.title LIKE "%{search}%"'
        query_cout += f' AND c.title LIKE "%{search}%"'

    query += ' GROUP BY c.id, c.title'

    if sort:
        query += f' ORDER BY c.id {sort}'
    
    query += f' LIMIT {limit} OFFSET {offset}'

    total_courses = database.query_count(query_cout)
    data = database.read_query(query)
    courses = [CourseResponceModelAdmin.from_query_result(*row) for row in data]
    return courses, total_courses


def get_students_who_rated_course(course_id:int, sort:str|None = None):
    '''Returns past and present students who rated the course'''

    query = '''SELECT u.id, u.mail, u.deactivated, r.rating 
            FROM students_has_ratings AS r 
            LEFT JOIN users AS u ON r.users_id = u.id
            WHERE courses_id = ?'''

    if sort:
        query += f' ORDER BY r.rating {sort}'

    params = (course_id,)
    data = database.read_query(query, params)
    return [StudentInfoForAdmin.query_result(*row) for row in data]


def change_course_status(course_id) -> str:
    '''Deactivate/activate course'''

    status = database.read_query(
        '''SELECT deactivated FROM courses WHERE id = ?''', (course_id,))
    
    if status[0][0] == 1: status = 0
    else: status = 1

    database.update_query(
        '''UPDATE courses SET deactivated = ? WHERE id = ?''', (status, course_id)) 
    
    if status == 0 : return 'activated'
    else: return 'deactivated'


def change_student_account_status(user_id) -> str:
    '''Deactivate/activate student account'''

    status = database.read_query(
        '''SELECT deactivated FROM users WHERE id = ?''', (user_id,))
    
    if status[0][0] == 1: status = 0
    else: status = 1

    database.update_query(
        '''UPDATE users SET deactivated = ? WHERE id = ?''', (status, user_id)) 

    if status == 0 : return 'activated'
    else: return 'deactivated'


def get_students_in_course(course_id) -> list[StudentInfoForAdmin]:
    '''Check number of students in a course'''
    
    count_students = database.query_count(
        '''SELECT COUNT(*) FROM students_has_courses WHERE courses_id = ?''', (course_id,)) 
    return count_students


def delete_student_from_course(course_id, user_id) -> None:
    '''Delete student if they are subscribed(1) ot pernding(3) to the course'''

    database.update_query(
        '''DELETE FROM students_has_courses WHERE users_id = ? AND courses_id = ? AND subscription = 1
        OR users_id = ? AND courses_id = ? AND subscription = 3''', (user_id, course_id, user_id, course_id)) 
    

def is_subscribed_or_pending(course_id, user_id) -> bool:
    '''Check if student is subscribed or pending to the course'''
    query = '''
            SELECT COUNT(*) AS is_subscribed
            FROM students_has_courses
            WHERE users_id = %s AND courses_id = %s AND subscription = 2
            OR users_id = %s AND courses_id = %s AND subscription = 3'''
    params = (user_id, course_id, user_id, course_id)
    data = database.query_count(query, params)
    return data

