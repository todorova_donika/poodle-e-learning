from data.models import Section, SectionForStudent
from data import database

def all(course_id:int, user_id:int, role:str, sort='asc', sort_by=None) -> list[Section] | list[SectionForStudent]:
    '''Returns all sections in a course'''

    if role == "student":
        query = '''
            SELECT id, title, content_type, description, external_info, visited
            FROM sections 
            LEFT JOIN students_has_sections ON sections_id = id and users_id = ?
            WHERE courses_id = ?'''
        if sort_by:
            query += f' ORDER BY title {sort}'
        else:
            query += f' ORDER BY id {sort}'

        params = (user_id, course_id)
        data = database.read_query(query, params)

        return [SectionForStudent.query_result(*row) for row in data]
    
    else:
        query = '''
            SELECT id, title, content_type, description, external_info
            FROM sections WHERE courses_id = ?'''
        if sort_by:
            query += f' ORDER BY title {sort}'
        else:
            query += f' ORDER BY id {sort}'

        params = (course_id,)
        data = database.read_query(query, params)
        return [Section.query_result(*row) for row in data]
    

def get_by_id(id:int) -> Section:
    '''Returns a section by id'''
    data = database.read_query(
        '''SELECT id, title, content_type, description, external_info 
        FROM sections WHERE id = ?''', (id,))
    return next((Section.query_result(*row) for row in data), None)


def is_section_in_course(course_id:int, section_id:int) -> bool:
    '''Checks if a section is in a course'''
    return any(database.read_query(
        '''SELECT * FROM sections WHERE courses_id = ? and id = ?''', (course_id, section_id)))


def create(course_id:int, section:Section) -> Section:
    '''Creates a new section'''
    new_id = database.insert_query(
        '''INSERT INTO sections(courses_id, title, content_type, description, external_info) VALUES (?,?,?,?,?)''', 
        (course_id, section.title, section.content_type, section.description, section.info))
    data = database.read_query(
        '''SELECT id, title, content_type, description, external_info FROM sections WHERE id = ?''', (new_id,))
    return next((Section.query_result(*row) for row in data))


def update(existing:Section, updated:Section) -> Section:
    '''Updates a section'''
    merged = Section(
        id = existing.id,
        title = updated.title or existing.title,
        content_type = updated.content_type or existing.content_type,
        description = updated.description or existing.description,
        info = updated.info or existing.info
        )
    
    database.update_query(
        '''UPDATE sections SET 
        title = ?, content_type = ?, description = ?, external_info = ? WHERE id = ?''',
        (merged.title, merged.content_type, merged.description, merged.info, existing.id))
    
    return merged


def delete(section_id) -> None:
    '''Deletes a section'''
    database.update_query(
        '''DELETE FROM sections WHERE id = ?''', (section_id,))

    

def visit_section(user_id:int, section_id:int) -> None:
    '''Marks that a student has visited a section'''
    exists = any(database.read_query(
        '''SELECT * FROM students_has_sections WHERE users_id = ? AND sections_id =?''', (user_id, section_id)))
    
    if exists == False: 
        database.insert_query(
            '''INSERT INTO students_has_sections(users_id, sections_id, visited) VALUES(?,?,?)''', (user_id, section_id, 1))
        
        
def track_course_progres(user_id:int, course_id:int) -> dict:
    '''Returns the progress of a student in a course'''

    all_sections = database.query_count(
        '''SELECT COUNT(*) FROM sections WHERE courses_id = ?''', (course_id,)) 
    
    visited_sections = database.query_count(
        '''SELECT COUNT(*) FROM students_has_sections 
        JOIN sections ON id = sections_id
        WHERE courses_id = ? AND users_id = ?''', (course_id, user_id))
    
    progress = (visited_sections*100)/all_sections
    progress_in_percentage = f'{progress:.2f}%'
    
    return {"progress":progress_in_percentage}
