from fastapi import Depends, HTTPException, status
from datetime import datetime, timedelta
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt
from config import settings


# Authentication
oauth2_scheme = OAuth2PasswordBearer(tokenUrl='users/login')

# SECRET_KEY = 'qN70S6oDTdy60vDj4KfkTizylgexLnqTzSjOTeC0QfvlzbMo3eR'
# ALGORITHM = "HS256"
# EXPIRATION_TIME_MINUTES = 60

SECRET_KEY = settings.secret_key
ALGORITHM = settings.algorithm
EXPIRATION_TIME_MINUTES = settings.access_token_expire_minutes


def create_access_token(data: dict) -> str:
    to_encode = data.copy()

    expire = datetime.utcnow() + timedelta(minutes=EXPIRATION_TIME_MINUTES)
    to_encode.update({"exp": expire})

    token = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)

    return token


def verify_token(token: str) -> dict:

    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        return payload

    except JWTError:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "bearer"},
        )


def get_current_user(token: str = Depends(oauth2_scheme)) -> dict:
    payload = verify_token(token)
    user_id = payload.get("user_id")
    role = payload.get("role")

    if user_id is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
            headers={"WWW-Authenticate": "bearer"},
        )
    
    return {"user_id": user_id, "role": role}


# Authorisation
def admin_only(token: str = Depends(oauth2_scheme)) -> int:
    payload = verify_token(token)
    role = payload.get("role")

    if role != "admin":
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Insufficient permissions to access this resource')
    
    user_id = payload.get("user_id")

    return user_id


def teacher_only(token: str = Depends(oauth2_scheme)) -> int:
    payload = verify_token(token)
    role = payload.get("role")

    if role != "teacher": 
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Insufficient permissions to access this resource')

    user_id = payload.get("user_id")

    return user_id


def student_only(token: str = Depends(oauth2_scheme)) -> int:
    payload = verify_token(token)
    role = payload.get("role")

    if role != "student": 
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Insufficient permissions to access this resource')

    user_id = payload.get("user_id")

    return user_id