from typing import Dict
from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.responses import JSONResponse
from common import auth
from data.models_users import AdminResponseModel, StudentResponseModel, StudentUpdate, TeacherResponseModel, TeacherUpdate, UserStudentRegister, UserTeacherRegister
from services import users_service


users_router = APIRouter(prefix='/users', tags=['User'])


@users_router.post('/students/register', status_code=201, response_model=StudentResponseModel, responses={
    201: {"description": "Successful Registration","content": {"application/json": {
                    "example": {"email": "george_georgeson@gmail.com",
                                 "role": "student",
                                 "deactivated": False,
                                 "first_name": "George",
                                 "last_name": "Georgeson"}}}},
    400: {"description": "User is Already Registered","content": {"application/json": {
                    "example": {"message":f'User with email johnjohnson@gmail.com is already registered.'}}}}})

def register_student(data: UserStudentRegister):
    '''Register a new student'''

    if users_service.exists(data.email):
        raise HTTPException(status_code=400, detail=f'User with email {data.email} is already registered.')

    student = users_service.create_student(data)

    return student


@users_router.post('/teachers/register', status_code=201, response_model=TeacherResponseModel, responses={
    201: {"description": "Successful Registration","content": {"application/json": {
                    "example": {"email": "johnjohnson@gmail.com",
                                 "role": "teacher",
                                 "deactivated": False,
                                 "first_name": "John",
                                 "last_name": "Johnson",
                                 "phone": "0895764324",
                                 "linked_in": "www.linkedin.com/in/john-johnson"}}}},
    400: {"description": "User is Already Registered","content": {"application/json": {
                    "example": {"message":f'User with email johnjohnson@gmail.com is already registered.'}}}}})

def register_teacher(data: UserTeacherRegister):
    '''Register a new teacher'''

    if users_service.exists(data.email):
        return JSONResponse(status_code=400, content=f'User with email {data.email} is already registered.')

    teacher = users_service.create_teacher(data)

    return teacher


@users_router.post('/login', response_model=Dict[str, str], responses={
    200: {"description": "Successful Login","content": {"application/json": {
                    "example": {"access_token": "InJvbGUiOiJ0ZWFjaGVyIiwiZXhwIjoxNjg1NzEzMzY2fQ.FX8ttSRFyuRWk2IUOZM0nphynqqAmpoxwFRjONeUDUQ",
                                 "token_type": "bearer"}}}},
    400: {"description": "Invalid password","content": {"application/json": {
                    "example": {"message":'Invalid password.'}}}},
    404: {"description": "Email not found","content": {"application/json": {
                    "example": {"message":'Email not found.'}}}}})

def login_user(data: OAuth2PasswordRequestForm=Depends()):
    '''Login a user'''

    exists = users_service.exists(data.username)

    if not exists:
        return JSONResponse(status_code=404, content={"message": 'User not found.'})
    
    user = users_service.login(data. username, data.password)

    if user:
        token = auth.create_access_token(data={"user_id": user.id, "email": user.email, "role": user.role})
        return {"access_token": token, "token_type": "bearer"}
    else:
        return JSONResponse(status_code=400, content={"message":'Invalid login data.'})


@users_router.get('/', response_model=TeacherResponseModel | StudentResponseModel | AdminResponseModel, responses={
    200: {"description": "Viewed Student/Teacher Profile","content": {"application/json": {
                    "example": {"email": "johnjohnson@gmail.com",
                                 "role": "teacher",
                                 "deactivated": False,
                                 "first_name": "John",
                                 "last_name": "Johnson",
                                 "phone": "0895764324",
                                 "linked_in": "www.linkedin.com/in/john-johnson"}}}}})

def view_user_account(current_user: dict = Depends(auth.get_current_user)):
    '''View a user's profile'''

    result = users_service.view(current_user["user_id"])

    if not result:
        raise HTTPException(status_code=404, detail='User not found.')

    return result


@users_router.put('/students', response_model=StudentResponseModel, responses={
    200: {"description": "Edited Profile","content": {"application/json": {
                    "example": {"email": "georgegeorgeson@gmail.com",
                                 "role": "student",
                                 "deactivated": False,
                                 "first_name": "George",
                                 "last_name": "Georgeson"}}}},
    400: {"description": "Did Not Make Changes to Profile","content": {"application/json": {
                    "example": {"message":'User is not a student or no new data was input.'}}}}})

def edit_student_account(data: StudentUpdate, current_user: dict = Depends(auth.get_current_user)):
    '''Edit a student's profile'''

    user_id = current_user["user_id"]
    student = users_service.edit_student(user_id, data)

    if student:
        return student
    else:
        raise HTTPException(status_code=400, detail='User is not a student or no new data was input.')


@users_router.put('/teachers', response_model=TeacherResponseModel, responses={
    200: {"description": "Edited Profile","content": {"application/json": {
                    "example": {"email": "johnjohnson@gmail.com",
                                 "role": "teacher",
                                 "deactivated": False,
                                 "first_name": "John",
                                 "last_name": "Johnson",
                                 "phone": "0895764324",
                                 "linked_in": "www.linkedin.com/in/john-johnson"}}}},
    400: {"description": "Did Not Make Changes to Profile","content": {"application/json": {
                    "example": {"message":'User is not a teacher or no new data was input.'}}}}})

def edit_teacher_account(data: TeacherUpdate, current_user: dict = Depends(auth.get_current_user)):
    '''Edit a teacher's profile'''

    user_id = current_user["user_id"]
    teacher = users_service.edit_teacher(user_id, data)

    if teacher:
        return teacher
    else:
        raise HTTPException(status_code=400, detail='User is not a teacher or no new data was input!')