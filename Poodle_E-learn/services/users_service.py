import hashlib
from data import database
from data.models_users import AdminResponseModel, StudentUpdate, TeacherUpdate, UserDatabase, UserStudentRegister, UserTeacherRegister, StudentDatabase, TeacherDatabase, StudentResponseModel, TeacherResponseModel

def create_student(student: UserStudentRegister) -> StudentResponseModel: 
    '''Creates a new student in the database'''
    student.password = _hash_password(student.password)
    role = 1

    query1 = '''INSERT INTO users (mail, password, role) VALUES (?,?,?)'''
    query2 = '''INSERT INTO students (users_id, first_name, lastname) VALUES (?,?,?)'''

    generated_id = database.insert_query(query1, (student.email, student.password, role))
    student.id = generated_id

    database.insert_query(query2, (student.id, student.first_name, student.last_name))
        
    return StudentResponseModel(email=student.email, deactivated=student.deactivated, first_name=student.first_name, last_name=student.last_name)


def create_teacher(teacher: UserTeacherRegister) -> TeacherResponseModel: 
    '''Creates a new teacher in the database'''
    teacher.password = _hash_password(teacher.password)
    role = 2


    query1 = 'INSERT INTO users(mail, password, role) VALUES (?,?,?)'
    query2 = 'INSERT INTO teachers(users_id, first_name, last_name, phone, linked_in) VALUES (?,?,?,?,?)'

    generated_id = database.insert_query(query1, (teacher.email, teacher.password, role))
    teacher.id = generated_id

    database.insert_query(query2, (teacher.id, teacher.first_name, teacher.last_name, teacher.phone, teacher.linked_in))

    return TeacherResponseModel(email=teacher.email, deactivated=teacher.deactivated, first_name=teacher.first_name, last_name=teacher.last_name, phone=teacher.phone, linked_in=teacher.linked_in)


def get_by_email(email: str) -> UserDatabase | None:
    '''Returns a user by email'''
    data = database.read_query(
        'SELECT id, mail, password, role, deactivated FROM users WHERE mail = ?',
        (email,))
    
    user = next((UserDatabase.from_query_result(*row) for row in data), None)

    return user


def get_by_id(id: int) -> UserDatabase | None:
    '''Returns a user by id'''
    data = database.read_query(
        'SELECT id, mail, password, role, deactivated FROM users WHERE id = ?',
        (id,))

    user = next((UserDatabase.from_query_result(*row) for row in data), None)

    return user


def get_student_by_id(id: int) -> StudentDatabase | None:
    '''Returns a student by id'''
    data = database.read_query(
        '''SELECT u.id, u.mail, u.password, u.role, u.deactivated, s.first_name, s.lastname
        FROM users as u
        JOIN students as s ON u.id = s.users_id
        WHERE u.id = ?''',
        (id,))
    
    student = next((StudentDatabase.from_query_result(*row) for row in data), None)

    return student


def get_teacher_by_id(id: int) -> TeacherDatabase | None:
    '''Returns a teacher by id'''
    data = database.read_query(
        '''SELECT u.id, u.mail, u.password, u.role, u.deactivated, t.first_name, t.last_name, t.phone, t.linked_in
        FROM users as u
        JOIN teachers as t ON u.id = t.users_id
        WHERE u.id = ?''',
        (id,))
    
    teacher = next((TeacherDatabase.from_query_result(*row) for row in data), None)

    return teacher


def edit_student(id: int, student_update: StudentUpdate) -> StudentResponseModel:
    '''Updates a student in the database'''
    student = get_student_by_id(id)

    if student is None:
        return None

    result_users = database.update_query(
        '''UPDATE users SET password = ?, deactivated = ? WHERE id = ?''',
        (_hash_password(student_update.password), student_update.deactivated, id))

    result_students = database.update_query(
        '''UPDATE students SET first_name = ?, lastname = ? WHERE users_id = ?''',
        (student_update.first_name, student_update.last_name, id))
    
    if result_users > 0 or result_students > 0:
        student.password = student_update.password
        student.deactivated = student_update.deactivated
        student.first_name = student_update.first_name
        student.last_name = student_update.last_name

        return StudentResponseModel(email=student.email, deactivated=student.deactivated, first_name=student.first_name, 
                                    last_name=student.last_name)
    else:
        return None


def edit_teacher(id: int, teacher_update: TeacherUpdate) -> TeacherResponseModel:
    '''Updates a teacher in the database'''
    teacher = get_teacher_by_id(id)

    if teacher is None:
        return None
    
    result_users = database.update_query(
        '''UPDATE users SET password = ?, deactivated = ? WHERE id = ?''',
        (_hash_password(teacher_update.password), teacher_update.deactivated, id))

    result_teachers = database.update_query(
        '''UPDATE teachers SET first_name = ?, last_name = ?, phone = ?, linked_in = ? WHERE users_id = ?''',
        (teacher_update.first_name, teacher_update.last_name, teacher_update.phone, teacher_update.linked_in, id))
    
    if result_users > 0 or result_teachers > 0:
        teacher.password = teacher_update.password
        teacher.deactivated = teacher_update.deactivated
        teacher.first_name = teacher_update.first_name
        teacher.last_name = teacher_update.last_name
        teacher.phone = teacher_update.phone
        teacher.linked_in = teacher_update.linked_in

        return TeacherResponseModel(email=teacher.email, deactivated=teacher.deactivated, first_name=teacher.first_name, 
                                    last_name=teacher.last_name, phone=teacher.phone, linked_in=teacher.linked_in)
    else:
        return None


def exists(email: str) -> bool:
    '''Checks if a user exists in the database'''
    return database.query_count('SELECT COUNT(*) FROM users WHERE mail = ?', (email,)) > 0


def login(username: str, password: str) -> UserDatabase | None:
    '''Checks for an existing user in the database if the password is correct'''
    user = get_by_email(username)
    pass_verif = _verify_password(password, user.password)

    return user if user and pass_verif else None


def view(id: int) -> StudentResponseModel | TeacherResponseModel | AdminResponseModel | None:
    '''Returns a user by id'''
    user = get_by_id(id)
    if user is None:
        return None
    
    if user.role == 'student':
        student = get_student_by_id(id)

        return StudentResponseModel(email=student.email, deactivated=student.deactivated, first_name=student.first_name, 
                                    last_name=student.last_name)

    if user.role == 'teacher':
        teacher = get_teacher_by_id(id)

        return TeacherResponseModel(email=teacher.email, deactivated=teacher.deactivated, first_name=teacher.first_name, 
                                    last_name=teacher.last_name, phone=teacher.phone, linked_in=teacher.linked_in)

    if user.role == 'admin':
        return AdminResponseModel(id=user.id, email=user.email, deactivated=user.deactivated)


def _hash_password(password: str) -> str:
    '''Hashes a password'''
    password_b = str.encode(password)
    sha256 = hashlib.sha256()
    sha256.update(password_b)
    password_hash = sha256.hexdigest()

    return password_hash


def _verify_password(password: str, hashed_password: str) -> bool:
    '''Verifies a password'''
    attempted_password = _hash_password(password)

    if attempted_password == hashed_password:
        return True
    
    return False

